# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from active_transfer import __version__

with open("requirements.txt") as f:
    REQUIREMENTS = f.read().splitlines()
setup(
    name="ActiveTransfer",
    version=__version__,
    url="https://gitlab.com/lazToum/active_trasnfer",
    license="MIT",
    author="Lazaros Toumanidis",
    author_email="laztoum@protonmail.com",
    description="Active & transfer learning combination",
    packages=find_packages(),
    long_description=open("README.md").read(),
    install_requires=REQUIREMENTS,
    python_requires=">=3.7",
    zip_safe=False,
)
