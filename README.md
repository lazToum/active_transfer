# ActiveTransfer
Active and transfer learning combination playground <br />
[pytorch](https://pytorch.org/) is used for train, validation and testing.<br />
An abstract class [(ml.py)](./active_transfer/ml.py) is used as a basis with train, test and sample functions,
<br /> with an initial implementation on binary image classification [(image/binary.py)](active_transfer/handlers/image/binary.py), using [torchvision](https://pytorch.org/docs/stable/torchvision/).
<br /> Least confidence, margin and entropy are used for uncertainty sampling
<br /> An [example](./example.py) is included where the above strategies and a random selection, can be tested, including a csv export. 
<br /> After collecting images of two classes, you can invoke:
<br />
`ACTIVE_TRANSFER_PEER_DATA=/path/to/dir/with/two/classes AT_DAEMON=1 AT_POSITIVE=ants AT_NEGATIVE=bees python3 example.py`
## TODO:
- [ ] implement more handlers on image (multi-class, object detection, image captioning)
- [ ] include diversity sampling methods
- [ ] audio related ml class implementation, using [torchaudio](https://pytorch.org/audio/)
- [ ] text related ml class implementation, using [torchtext](https://pytorch.org/text/)

## Licence
[Apache License 2](LICENSE)
