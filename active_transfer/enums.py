# -*- coding: utf-8 -*-
from __future__ import annotations
from enum import Enum
from typing import Optional


class Phase(str, Enum):
    """The names of our phase directories."""

    Train = "train"
    Val = "val"
    Sample = "pool"
    Test = "test"

    @staticmethod
    def from_string(string: str) -> Optional[Phase]:
        """Phase from string."""
        if string.lower() == "train":
            return Phase(Phase.Train)
        if string.lower() == "val":
            return Phase(Phase.Val)
        if string.lower() == "test":
            return Phase(Phase.Test)
        if string.lower() == "pool" or string.lower() == "sample":
            return Phase(Phase.Sample)
        return None

    def __repr__(self):
        """To string."""
        return f"<{self.name}.{self.value}>"


class QueryStrategy(str, Enum):
    """The way we decide for the most informative samples."""

    Random: str = "Random Sampling"
    LeastConfidence: str = "Least Confidence Sampling"
    Margin: str = "Margin Sampling"
    Entropy: str = "Max Entropy Sampling"

    @staticmethod
    def from_string(string: str) -> Optional[QueryStrategy]:
        """QueryStrategy from string."""
        if string.lower() == "entropy":
            return QueryStrategy(QueryStrategy.Entropy)
        if string.lower() == "margin":
            return QueryStrategy(QueryStrategy.Margin)
        if string.lower() == "random":
            return QueryStrategy(QueryStrategy.Random)
        if string.lower() == "leastconfidence":
            return QueryStrategy(QueryStrategy.LeastConfidence)
        return None

    def __repr__(self):
        """To string."""
        return f"<{self.name}.{self.value}>"


class Category(str, Enum):
    """Task Category."""

    Image = "image"
    Audio = "audio"
    Text = "text"
    Other = "other"

    @staticmethod
    def from_string(string: str) -> Optional[Category]:
        """Category from string."""
        if string.lower() == "image":
            return Category(Category.Image)
        if string.lower() == "audio":
            return Category(Category.Audio)
        if string.lower() == "text":
            return Category(Category.Text)
        if string.lower() == "other":
            return Category(Category.Other)
        return None


class SubCategory(str, Enum):
    """Task Subcategories."""

    Binary = "Binary Classification"
    Multi = "Multi-class Classification"
    Detect = "Object Detection"
    Caption = "Object Captioning"

    @staticmethod
    def from_string(string: str) -> Optional[SubCategory]:
        """Subcategory from string."""
        if string.lower() == "binary":
            return SubCategory(SubCategory.Binary)
        if string.lower() == "multi":
            return SubCategory(SubCategory.Multi)
        if string.lower() == "detect":
            return SubCategory(SubCategory.Detect)
        if string.lower() == "caption":
            return SubCategory(SubCategory.Caption)
        return None
