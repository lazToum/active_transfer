# -*- coding: utf-8 -*-
import json
import os
import sys
import csv
import random
from typing import Union, List, Optional, Dict

from .enums import Phase


def here(package_path: bool = False) -> str:
    """Path of the file calling this function.

    Get the path the current running script or this file(.py|.ipynb)
    resides.
    """
    try:
        return os.path.realpath(
            os.path.dirname(__file__ if package_path else sys.argv[0])
        )
    except NameError:
        # inside notebook?
        return os.path.realpath(".")


def weighted_sizes(count: int, weights: List[int]) -> List[int]:
    """Weighted sizes."""
    total = sum(x for x in weights)
    if count > total:
        return weights
    if total < 2:
        return [max(weights)]
    partials = [int(count * (y / total)) for y in weights]
    partials_total = sum(partials)
    diff = count - partials_total
    if diff != 0:
        # we missed 1 item: int(float)
        if diff > 0:
            # add 1 to the one with the fewer items
            partial_min = min(partials)
            partial_index = partials.index(partial_min)
            partials[partial_index] += 1
        else:
            partial_max = max(partials)
            partial_index = partials.index(partial_max)
            partials[partial_index] += 1
    return partials


def weighted_dir_items(
    root_dir: Union[str, os.PathLike],
    names: List[str],
    count: int,
    ordered: Optional[bool] = False,
    random_seed: Optional[int] = None,
) -> Dict[str, List[str]]:
    """Get number of items with weights.

    :param root_dir: The root directory
    :param names: The names of the sub-folders
    :param count: The number of items to get
    :param ordered: Sort listdir or not
    :param random_seed: optional random seed to shuffle the result
    :return:
    """
    ret = {}
    if not os.path.exists(root_dir):
        return ret
    # {
    #   "class_a": "/home/user/root_dir/class_a",
    #   "class_b": "/home/user/root_dir/class_b",
    #   "class_c": "/home/user/root_dir/class_c",
    # }
    if random_seed is not None:
        random.seed(random_seed)
    sub_dirs = {x: os.path.join(root_dir, x) for x in names}
    sub_dirs = {x: y for x, y in sub_dirs.items() if os.path.exists(y)}
    sub_dirs_count = len(sub_dirs)
    if sub_dirs_count < 2:
        if sub_dirs_count == 0:
            return ret
        class_name, folder = list(sub_dirs.items())[0]
        items = (
            os.listdir(folder)[:count] if not ordered else sorted(os.listdir(folder))
        )
        if random_seed is None:
            ret[class_name] = items[:count]
        else:
            random.shuffle(items)
            ret[class_name] = items[:count]
        return ret
    files = {
        x: os.listdir(y) if not ordered else sorted(os.listdir(y))
        for x, y in sub_dirs.items()
    }
    sizes = {x: len(y) for x, y in files.items()}
    total = sum(x for x in sizes.values())
    if total < 2:
        return files
    keys = list(sizes.keys())
    values = list(sizes.values())
    partial_sizes = weighted_sizes(count, values)
    ret = {}
    for index, size in enumerate(partial_sizes):
        key = keys[index]
        if random_seed is not None:
            random.shuffle(files[key])
        ret[key] = files[key][:size]
    return ret


class AtStorage(object):
    """Storage class."""

    root_dir: Union[str, os.PathLike]
    class_names: Optional[List[str]]
    _random_seed: int = 0
    phases: List[Phase] = [Phase.Test, Phase.Train, Phase.Val, Phase.Sample]

    def __init__(
        self,
        base_path: Union[str, os.PathLike],
        class_names: Optional[List[str]] = None,
    ):
        """Class Initialization."""
        self.root_dir = os.path.realpath(base_path)
        if not os.path.exists(self.root_dir):
            raise FileNotFoundError("Invalid root directory")
        self.class_names = class_names
        self.setup_dirs()

    def setup_dirs(self):
        """Folders initialization.

        Make sure the required (train, val, test, pool) dirs and their
        sub- dirs exist.
        """
        for phase in self.phases:
            _phase_dir = os.path.join(self.root_dir, phase.value)
            os.makedirs(_phase_dir, exist_ok=True)
            if self.class_names is not None and phase != Phase.Sample:
                for _class_name in self.class_names:
                    _sub_dir = os.path.join(_phase_dir, _class_name)
                    os.makedirs(_sub_dir, exist_ok=True)
                    os.chmod(_sub_dir, 0o777)

    def __path_dir(
        self, phase: Phase, only_path=False
    ) -> Optional[Union[str, List[str]]]:
        _path = os.path.join(self.root_dir, phase.value)
        if not os.path.exists(_path):
            return None if only_path else []
        if only_path:
            return _path
        return os.listdir(_path)

    def train_dir(self, only_path=False) -> Optional[Union[str, List[str]]]:
        """Path of train items."""
        return self.__path_dir(Phase.Train, only_path)

    def val_dir(self, only_path=False) -> Optional[Union[str, List[str]]]:
        """Path of validation items."""
        return self.__path_dir(Phase.Val, only_path)

    def test_dir(self, only_path=False) -> Optional[Union[str, List[str]]]:
        """Path of test items."""
        return self.__path_dir(Phase.Test, only_path)

    def samples_dir(self, only_path=False) -> Optional[Union[str, List[str]]]:
        """Path of sample items."""
        return self.__path_dir(Phase.Sample, only_path)

    @staticmethod
    def write_csv(csv_file: Union[str, os.PathLike], entries: dict, mode: str = "a"):
        """Write (dict) entries to csv file."""
        fieldnames = list(entries.keys())
        _mode = mode
        if not os.path.exists(csv_file):
            _mode = "w"
        try:
            with open(csv_file, _mode, newline="") as _file:
                writer = csv.DictWriter(_file, fieldnames=fieldnames)
                if _mode == "w":
                    writer.writeheader()
                _row = {}
                for _field_name in writer.fieldnames:
                    _row[_field_name] = entries.get(_field_name, "NA")
                writer.writerow(_row)
        except Exception as e:
            print(e)
            raise e

    @staticmethod
    def read_csv(csv_file: Union[str, os.PathLike]) -> Optional[dict]:
        """Read entries from csv file.

        :return: The contents of the csv file as a dict if the file exists
        """
        _data = {}
        if os.path.exists(csv_file):
            with open(csv_file, newline="") as csv_read:
                reader = csv.DictReader(csv_read)
                for _key in reader.fieldnames:
                    _data[_key] = []
                for row in reader:
                    for key, val in row.items():
                        _data[key].append(val)
            return _data
        return None

    @staticmethod
    def write_json(json_file: Union[str, os.PathLike], entries: dict, mode: str = "w+"):
        """Write (dict) entries to json file.

        :return: True if data are written, false if not
        """
        try:
            with open(json_file, mode=mode) as _file:
                _file.write(json.dumps(entries))
            # os.chmod(json_file, 0o777)
        except Exception as error:
            print(error)
            raise error

    @staticmethod
    def read_json(json_file: Union[str, os.PathLike]):
        """Read entries from json file.

        :return: The contents of the json file
        """
        try:
            with open(json_file) as f:
                return json.load(f)
        except Exception as error:
            print(error)
            raise error
