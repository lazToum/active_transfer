# -*- coding: utf-8 -*-
from .ml import AtMl
from .scenario import AtScenario
from .storage import here, AtStorage
from .handlers import get_handler
import os
from datetime import datetime
from typing import List, NoReturn, Optional, Type
import torch


class ActiveTransfer(AtStorage):
    """The main class."""

    _model_path: str = os.path.join(
        here(), "%s.pt" % datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    )
    _scenario: AtScenario
    _handler: Optional[AtMl] = None

    def __init__(
        self,
        root_dir: str,
        model: torch.nn.Module,
        scenario: AtScenario,
        class_names: Optional[List[str]] = None,
        **kwargs
    ):
        """Class initialisation.

        :param root_dir: Root directory
        :param classes: Class names (for positive/negative)
            and the relevant folders, relative to root dir
        """
        _handler: Optional[Type[AtMl]] = get_handler(scenario)
        if _handler is None:
            raise NotImplementedError
        self._handler = _handler(root_dir, class_names, **kwargs)
        class_names = self._handler.class_names
        self._scenario = scenario
        self._model = model
        model_path = kwargs.get("model_path", None)
        if model_path is not None:
            os.makedirs(os.path.dirname(model_path), exist_ok=True)
            self._model_path = model_path
        super(ActiveTransfer, self).__init__(root_dir, class_names)
        _storage: AtStorage = self
        self._handler.storage = _storage

    @property
    def device(self):
        """Run in cpu or gpu."""
        return self._handler.device

    @property
    def scenario(self):
        """Settings."""
        return self._scenario

    @property
    def model(self) -> torch.nn.Module:
        """Torch module."""
        return self._model

    @model.setter
    def model(self, value: torch.nn.Module) -> NoReturn:
        self._model = value

    @property
    def can_train(self) -> bool:
        """Check if we can train our model."""
        return self._handler.can_train(
            self._scenario.train_size
            + self._scenario.val_size
            + self._scenario.test_size
        )

    @property
    def can_test(self) -> bool:
        """Check if we can test our model."""
        return self._handler.can_test(self._scenario.test_size)

    @property
    def have_samples(self) -> bool:
        """Check if we have enough samples to ask for labeling."""
        return self._handler.have_samples(self._scenario.append_size)

    @property
    def classes(self) -> List[str]:
        """Our class names."""
        return [x for x in self.class_names] if self.class_names is not None else []

    @property
    def model_path(self) -> str:
        """Path where we store the model(s)."""
        return self._model_path

    @model_path.setter
    def model_path(self, value) -> NoReturn:
        try:
            os.makedirs(os.path.dirname(value), exist_ok=True)
            self._model_path = value
        except Exception as e:
            print(e)
            raise e

    def get_samples(self, count: int, **kwargs):
        """Get new samples."""
        return self._handler.get_samples(
            count, self._scenario.strategy, model=self._model, **kwargs
        )

    def train(self, criterion, optimizer, scheduler, **kwargs):
        """Model training."""
        load_if_exists = kwargs.get("load_if_exists", False)
        model = (
            torch.load(self._model_path)
            if (
                load_if_exists
                and self.model_path is not None
                and os.path.exists(self._model_path)
                and os.path.isfile(self._model_path)
            )
            else self._model
        )
        return self._handler.train(
            model,
            criterion=criterion,
            optimizer=optimizer,
            scheduler=scheduler,
            **kwargs
        )

    def test(self, **kwargs) -> dict:
        """Model testing."""
        return self._handler.test(self._model, **kwargs)
