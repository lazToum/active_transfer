# -*- coding: utf-8 -*-

import os
from abc import abstractmethod, ABCMeta
from typing import Tuple, List, Dict, Union, Optional, Any
import torch
import torchvision
from .enums import Phase, QueryStrategy
from .storage import AtStorage


class AtMl(metaclass=ABCMeta):
    """ML part(abstract class)."""

    root_dir: Union[str, os.PathLike]
    class_names: List[str]
    _device: torch.device
    _transforms: Dict[Phase, Optional[torchvision.transforms.Compose]] = {
        Phase.Train: None,
        Phase.Val: None,
        Phase.Sample: None,
        Phase.Test: None,
    }
    _random_seed: int = 0
    _batch_size = 32
    _num_workers = 4
    _num_epochs: int = 20
    _notifier: callable = print
    _storage: AtStorage

    def __init__(
        self, root_dir: Union[str, os.PathLike], class_names: List[str], **kwargs
    ):
        """Class Initialization."""
        self.root_dir = root_dir
        self.class_names = class_names
        device_string = kwargs.get("device", None)  # cuda:0, cpu
        if device_string is not None:
            self.device = torch.device(device_string)
        else:
            self._device = torch.device(
                "cuda:0" if torch.cuda.is_available() else "cpu"
            )
        self._batch_size = int(kwargs.get("batch_size", self._batch_size))
        self._num_workers = int(kwargs.get("num_workers", self._num_workers))
        self._num_epochs = int(kwargs.get("num_epochs", self._num_epochs))
        self._notifier = kwargs.get("notifier", self._notifier)
        self._random_seed: int = int(kwargs.get("random_seed", self._random_seed))
        transforms: Dict[Phase, Optional[torchvision.transforms.Compose]] = kwargs.get(
            "transforms", {}
        )
        for key, value in self._transforms.items():
            self._transforms[key] = transforms.get(key, value)

    @property
    def storage(self) -> AtStorage:
        """Storage module."""
        return self._storage

    @storage.setter
    def storage(self, value: AtStorage):
        self._storage = value

    @property
    def device(self) -> torch.device:
        """Run in cpu or gpu."""
        return self._device

    @device.setter
    def device(self, value: torch.device):
        """Run in cpu or gpu."""
        self._device = value

    @abstractmethod
    def can_train(self, required: any) -> bool:
        """Check if we can train our model."""
        raise NotImplementedError

    @abstractmethod
    def train(
        self,
        model: torch.nn.Module,
        criterion,
        optimizer,
        scheduler,
        notifier: callable = print,
        **kwargs
    ) -> Tuple[
        torch.nn.Module,
        Union[
            Dict[Phase, dict],
            Dict[Any, Dict[str, Union[Union[float, int, int, float, bool, bool], Any]]],
        ],
        float,
    ]:
        """Model training.

        :param model The torch model
        :param criterion: the loss criterion
        :param optimizer: the model optimizer
        :param scheduler: learning rate scheduler
        :param notifier: the notifier function (e.g. print)
        :return: The model, the last epoch and the min loss
        """
        raise NotImplementedError

    @abstractmethod
    def can_test(self, required: any) -> bool:
        """Check if we can test our model.

        :param required: What to compare with
        :return: If we have enough reqs to test the model
        """
        raise NotImplementedError

    @abstractmethod
    def test(
        self, model: torch.nn.Module, notifier: callable = print, **kwargs
    ) -> dict:
        """Test the current state of the model.

        :param model: The model
        :param notifier: Notify about the status
        :return The test results
        """
        raise NotImplementedError

    @abstractmethod
    def have_samples(self, required: any) -> bool:
        """Do we have enough samples."""
        raise NotImplementedError

    @abstractmethod
    def margin_confidence_sampling(
        self, model: torch.nn.Module, count, **kwargs
    ) -> List[Tuple[str, torch.Tensor]]:
        """Margin Confidence sampling.

        :param model: the torch model
        :param count: How many items
        :param kwargs: Optional extra arguments
        """
        raise NotImplementedError

    @abstractmethod
    def least_confidence_sampling(
        self, model: torch.nn.Module, count: int, **kwargs
    ) -> List[Tuple[str, torch.Tensor]]:
        """Least confidence sampling.

        :param model: the torch model
        :param count: How many items
        :param kwargs: Optional extra arguments
        """
        raise NotImplementedError

    @abstractmethod
    def entropy_sampling(
        self, model: torch.nn.Module, count, **kwargs
    ) -> List[Tuple[str, torch.Tensor]]:
        """Max entropy sampling.

        :param model: the torch model
        :param count: How many items
        :param kwargs: Optional extra arguments
        """
        raise NotImplementedError

    @abstractmethod
    def random_sampling(self, count: int, **kwargs):
        """Random Sampling.

        :param count: How many items
        :param kwargs: Optional extra arguments (e.g. random seed)
        """
        raise NotImplementedError

    @abstractmethod
    def get_samples(
        self,
        count: int,
        strategy: QueryStrategy,
        model: torch.nn.Module = None,
        **kwargs
    ):
        """Get samples from the samples pool folder.

        @:param count: how many samples
        @:param strategy: how to get these samples
        """
        raise NotImplementedError
