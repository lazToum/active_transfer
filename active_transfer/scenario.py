# -*- coding: utf-8 -*-
try:
    from typing import TypedDict  # python3.8
except ImportError:
    from typing import Dict as TypedDict  # python < 3.8 fallback
from .enums import QueryStrategy, Category, SubCategory


class PhaseSize(TypedDict):
    """Dictionary with the number of elements to use for each phase."""

    train: int
    test: int
    val: int


class AtScenario(object):
    """Scenario settings.

    A Scenario (to run) containing:
        the initial train size, the number of items we should append before retaining,
        the scenario category:
            Image
            Audio
            Text
            Other,
        the scenario subcategory:
            Binary Classification
            Multi-class Classification
            Object Detection
            Object Captioning
        the way we should sample files to append to the
        train set:
            Random Sampling
            Least Confidence Sampling
            Margin Sampling
            Entropy Sampling
    """

    _train_size: int
    _test_size: int
    _val_size: int
    _append_size: int
    _category: Category
    _subcategory: SubCategory
    _strategy: QueryStrategy = QueryStrategy(QueryStrategy.Random)

    def __init__(
        self,
        phase_sizes: PhaseSize,
        append_size: int,
        category: Category,
        subcategory: SubCategory,
        **kwargs
    ):
        """Class initialization."""
        self._train_size = phase_sizes["train"]
        self._test_size = phase_sizes["test"]
        self._val_size = phase_sizes["val"]
        self._append_size = append_size
        self._category = category
        self._subcategory = subcategory
        self._strategy = QueryStrategy(kwargs.get("strategy", QueryStrategy.Random))

    @property
    def category(self) -> Category:
        """Our scenario's base type (Image|Audio|Text|Other)."""
        return self._category

    @category.setter
    def category(self, value: Category):
        """Set the scenario's base type."""
        self._category = value

    @property
    def subcategory(self) -> SubCategory:
        """Our scenario's subtype (Binary|Multi-class|Annotation|Detection)."""
        return self._subcategory

    @subcategory.setter
    def subcategory(self, value: SubCategory):
        """Set the scenario's subcategory."""
        self._subcategory = value

    @property
    def train_size(self) -> int:
        """Train size of the scenario."""
        return self._train_size

    @property
    def val_size(self) -> int:
        """Size of the scenario's validation items."""
        return self._val_size

    @property
    def test_size(self) -> int:
        """Test size of the scenario."""
        return self._test_size

    @property
    def append_size(self) -> int:
        """Size of the files to append after each scenario's loop."""
        return self._append_size

    @property
    def strategy(self) -> QueryStrategy:
        """How we should choose the new samples."""
        return self._strategy

    def to_name(self) -> str:
        """Representation to use for file name."""
        _strategy_str = "rd"
        if self._strategy == QueryStrategy.LeastConfidence:
            _strategy_str = "lc"
        if self._strategy == QueryStrategy.Entropy:
            _strategy_str = "en"
        if self._strategy == QueryStrategy.Margin:
            _strategy_str = "mg"
        return "{}_strategy_{}_train_{}_append_{} category {}_subcategory".format(
            _strategy_str,
            self._train_size,
            self._append_size,
            self._category.name,
            self._subcategory.name,
        )

    def __str__(self) -> str:
        """To string."""
        return "Strategy: {}, Train: {}, Append: {}, Category: {}, Subcategory: {}".format(
            self._strategy.value,
            self._train_size,
            self._append_size,
            self._category.value,
            self._subcategory.value,
        )
