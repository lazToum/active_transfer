# -*- coding: utf-8 -*-
from ..enums import Category, SubCategory
from ..handlers.image import ImageBinary
from ..ml import AtMl
from ..scenario import AtScenario
from typing import Optional, Type


def get_handler(scenario: AtScenario) -> Optional[Type[AtMl]]:
    """Get Ml Handler based on the scenario."""
    category = scenario.category
    subcategory = scenario.subcategory
    if category == Category.Image:
        return _image(subcategory)
    if category == Category.Audio:
        return _audio(subcategory)
    if category == Category.Text:
        return _text(subcategory)
    return None


def _image(subcategory) -> Optional[Type[AtMl]]:
    """"Image Subcategory."""
    return ImageBinary if subcategory == SubCategory.Binary else None


def _audio(_):
    """"Audio Subcategory."""
    raise NotImplementedError


def _text(_):
    """"Text Subcategory."""
    raise NotImplementedError


__all__ = ["get_handler", "ImageBinary"]
