# -*- coding: utf-8 -*-s
import math
import os
import time
from typing import Tuple, Union, List, Optional, Dict, Any
import copy
from numpy import random
import torch
import torchvision
import torch.nn.functional as f
from torch.utils.data.dataset import Dataset
from torch.utils.data.dataloader import DataLoader
from torchvision.datasets.folder import default_loader

from ...enums import Phase, QueryStrategy
from ...ml import AtMl


class ImageBinarySamplesDataset(Dataset):
    """Samples dataset (no/unknown classes)."""

    def __init__(self, root_dir, transform=None):
        """Class Initialization."""
        self.root_dir = root_dir
        self.transform = transform
        self.items = [os.path.join(root_dir, x) for x in sorted(os.listdir(root_dir))]
        self.loader = default_loader

    def __len__(self):
        """Items length."""
        return len(self.items)

    def __getitem__(self, index):
        """Get item form index."""
        img_path = self.items[index]
        sample = self.loader(img_path)
        if self.transform is not None:
            sample = self.transform(sample)
        return sample, img_path


class ImageBinary(AtMl):
    """Binary Classification Image handler."""

    positive: str
    negative: str

    def __init__(
        self,
        root_dir: Union[str, os.PathLike],
        class_names: Optional[List[str]],
        **kwargs
    ):
        """Class Initialization."""
        if class_names is None:
            if len(os.listdir(root_dir)) != 2:
                raise ValueError("Cannot get the two binary classes")
        elif len(class_names) != 2:
            raise ValueError("Only two binary classes are allowed")
        _class_names = (
            class_names if class_names is not None else sorted(os.listdir(root_dir))
        )
        self.class_names = _class_names
        self.positive = _class_names[0]
        self.negative = _class_names[1]
        super(ImageBinary, self).__init__(root_dir, _class_names, **kwargs)

    def _train_epoch(self, model: torch.nn.Module, min_loss, best_model_wts, **kwargs):
        phases = kwargs.get("phases")
        dataloaders = kwargs.get("dataloaders")
        dataset_sizes = kwargs.get("dataset_sizes")
        optimizer = kwargs.get("optimizer")
        scheduler = kwargs.get("scheduler")
        criterion = kwargs.get("criterion")
        epoch_stats = {}

        # Each epoch has a training and validation phase
        for phase in phases:
            is_train = phase == Phase.Train
            if is_train:
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss: float = 0.0
            running_corrects: torch.Tensor = torch.tensor(0)

            # Iterate over data.
            for _, (inputs, labels) in enumerate(dataloaders[phase]):
                inputs = inputs.to(self._device)
                labels = labels.to(self._device)

                # zero the parameter gradients
                optimizer.zero_grad()
                with torch.set_grad_enabled(is_train):
                    outputs = model(inputs)
                    _, predictions = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)
                    # print(loss)

                    # backward + optimize only if in train
                    if is_train:
                        loss.backward()
                        optimizer.step()
                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(predictions == labels.data)
            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]
            _acc = epoch_acc.item()
            self._notifier(
                "{} loss: {:.4f} acc: {:.4f}".format(phase, epoch_loss, _acc),
                phase=phase.name,
                epoch_loss=epoch_loss,
                epoch_acc=_acc,
            )

            if is_train:
                scheduler.step()
            elif epoch_loss < min_loss:
                min_loss = float(epoch_loss)
                # deep copy the model
                best_model_wts = copy.deepcopy(model.state_dict())
            epoch_stats[phase] = {"loss": epoch_loss, "acc": _acc}
        return model, optimizer, scheduler, best_model_wts, min_loss, epoch_stats

    @staticmethod
    def _update_test_stats(data, target, predictions, **kwargs):
        true_pos = kwargs.get("true_pos")
        true_neg = kwargs.get("true_neg")
        false_pos = kwargs.get("false_pos")
        false_neg = kwargs.get("false_neg")
        for j in range(data.size(0)):
            expectation = target[j].item()
            prediction = predictions[j].item()
            positive = prediction == 1
            true = prediction == expectation
            if true and positive:
                true_pos += 1
            if true and not positive:
                true_neg += 1
            if not true and positive:
                false_pos += 1
            if not true and not positive:
                false_neg += 1
        return true_pos, true_neg, false_pos, false_neg

    def can_test(self, required: int) -> bool:
        """Check if we can test our model.

        :return: If we have enough reqs to test the model
        """
        test_files = self.storage.test_dir()
        return (
            sum(
                len(os.listdir(os.path.join(self.root_dir, Phase.Train.value, x)))
                for x in test_files
            )
            > required
        )

    def can_train(self, required: int) -> bool:
        """Check if we can train our model.

        :return: If we have enough reqs to start training
        """
        train_files = self.storage.train_dir()
        val_files = self.storage.val_dir()
        test_files = self.storage.test_dir()
        train_size = sum(
            len(os.listdir(os.path.join(self.root_dir, Phase.Train.value, x)))
            for x in train_files
        )
        val_size = sum(
            len(os.listdir(os.path.join(self.root_dir, Phase.Val.value, x)))
            for x in val_files
        )
        test_size = sum(
            len(os.listdir(os.path.join(self.root_dir, Phase.Test.value, x)))
            for x in test_files
        )
        return train_size + val_size + test_size >= required

    def have_samples(self, required: int) -> bool:
        """Do we have enough samples."""
        return len(self.storage.samples_dir()) > required

    def train(
        self,
        model: torch.nn.Module,
        criterion,
        optimizer,
        scheduler,
        notifier: callable = print,
        **kwargs
    ) -> Tuple[
        torch.nn.Module,
        Union[
            Dict[Phase, dict],
            Dict[Any, Dict[str, Union[Union[float, int, int, float, bool, bool], Any]]],
        ],
        float,
    ]:
        """Model training."""
        num_epochs = kwargs.get("num_epochs", self._num_epochs)
        batch_size: int = kwargs.get("batch_size", self._batch_size)
        num_workers: int = kwargs.get("num_workers", self._num_workers)
        random_seed: int = kwargs.get("random_seed", self._random_seed)
        torch.manual_seed(random_seed)
        phases: List[Phase] = [Phase.Train, Phase.Val]
        data_transforms: Dict[Phase, torchvision.transforms.Compose] = {
            Phase.Train: self._transforms[Phase.Train],
            Phase.Val: self._transforms[Phase.Val],
        }
        image_dataset = {
            x: torchvision.datasets.ImageFolder(
                os.path.join(self.root_dir, x), data_transforms[x]
            )
            for x in phases
        }
        dataloaders = {
            x: DataLoader(
                image_dataset[x],
                shuffle=x is Phase.Train,
                batch_size=batch_size,
                num_workers=num_workers,
            )
            for x in phases
        }
        dataset_sizes = {x: len(image_dataset[x]) for x in phases}
        best_model_wts = copy.deepcopy(model.state_dict())
        min_loss: float = 1.0
        last_epoch = {Phase.Train: {}, Phase.Val: {}}
        since = time.time()
        for epoch in range(self._num_epochs):
            self._notifier(
                "Epoch {}/{}".format(epoch + 1, self._num_epochs),
                state={"epoch": epoch + 1, "total": self._num_epochs},
            )
            self._notifier("-" * 10)
            (
                model,
                optimizer,
                scheduler,
                best_model_wts,
                min_loss,
                epoch_stats,
            ) = self._train_epoch(
                model,
                min_loss,
                best_model_wts,
                phases=phases,
                dataloaders=dataloaders,
                dataset_sizes=dataset_sizes,
                scheduler=scheduler,
                optimizer=optimizer,
                criterion=criterion,
            )
            if epoch == num_epochs - 1:
                last_epoch = epoch_stats
        time_elapsed = time.time() - since
        self._notifier(
            "Training complete in {:.0f}m {:.0f}s".format(
                time_elapsed // 60, time_elapsed % 60
            ),
            "Min val Loss: {:4f}".format(min_loss),
            duration=time_elapsed,
            min_loss=min_loss,
        )

        # load best model weights
        model.load_state_dict(best_model_wts)
        return model, last_epoch, min_loss

    def test(self, model: torch.nn.Module, **kwargs) -> dict:
        """Model testing."""
        batch_size: int = kwargs.get("batch_size", self._batch_size)
        num_workers: int = kwargs.get("num_workers", self._num_workers)
        test_folder: str = Phase.Test.value
        model.eval()
        true_pos: int = 0
        true_neg: int = 0
        false_pos: int = 0
        false_neg: int = 0
        test_loss: float = 0.0
        correct: float = 0
        test_transform = self._transforms[Phase.Test]
        test_dataset = torchvision.datasets.ImageFolder(
            os.path.join(self.root_dir, test_folder), test_transform
        )
        test_loader: DataLoader = DataLoader(
            test_dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers
        )
        total_length: int = len(test_loader.dataset)
        with torch.no_grad():
            for i, (data, target) in enumerate(test_loader):
                data, target = data.to(self.device), target.to(self.device)
                outputs = model(data)
                _, predictions = torch.max(outputs, 1)
                test_loss += f.cross_entropy(outputs, target).item() * data.size(0)
                correct += predictions.eq(target.view_as(predictions)).sum().item()
                (true_pos, true_neg, false_pos, false_neg) = self._update_test_stats(
                    data,
                    target,
                    predictions,
                    true_pos=true_pos,
                    true_neg=true_neg,
                    false_pos=false_pos,
                    false_neg=false_neg,
                )

        test_loss /= len(test_loader.dataset)
        return {
            "test_loss": test_loss,
            "test_acc": correct / total_length,
            "test_total": total_length,
            "test_correct": correct,
            "test_true_pos": true_pos,
            "test_true_neg": true_neg,
            "test_false_pos": false_pos,
            "test_false_neg": false_neg,
        }

    def __sample(self, model, output_handler, **kwargs):
        batch_size: int = kwargs.get("batch_size", self._batch_size)
        num_workers: int = kwargs.get("num_workers", self._num_workers)
        samples_dir = self.storage.samples_dir(True)
        model.eval()
        samples_dataset = ImageBinarySamplesDataset(
            samples_dir, self._transforms[Phase.Sample]
        )
        samples_loader: DataLoader = DataLoader(
            samples_dataset,
            batch_size=batch_size,
            shuffle=True,
            num_workers=num_workers,
        )
        device = self._device
        with torch.no_grad():
            for i, (image_data, image_paths) in enumerate(samples_loader):
                data_size = image_data.size(0)
                image_data = image_data.to(device)
                outputs = model(image_data)
                output_handler(outputs, image_paths, data_size)

    def margin_confidence_sampling(
        self, model: torch.nn.Module, count, **kwargs
    ) -> List[Tuple[str, torch.Tensor]]:
        """Margin Confidence sampling."""
        img_margins = []

        def handler(outputs, img_batch, data_size):
            """Handle samples model output."""
            for j in range(data_size):
                probabilities = torch.softmax(outputs[j], 0)
                margin = max(probabilities) - min(probabilities)
                img_margins.append((img_batch[j], margin))

        self.__sample(model, handler)
        img_margins.sort(key=lambda x: x[1])
        return img_margins[:count]

    def least_confidence_sampling(
        self, model: torch.nn.Module, count: int, **kwargs
    ) -> List[Tuple[str, torch.Tensor]]:
        """Least confidence sampling."""
        confidences = []

        def handler(outputs, img_batch, data_size):
            """Handle samples model output.

            :param outputs:
            :param img_batch:
            :param data_size:
            """
            for j in range(data_size):
                _img = img_batch[j]
                probabilities = torch.softmax(outputs[j], 0)
                confidence = torch.max(probabilities)
                confidences.append((_img, confidence))

        self.__sample(model, handler, **kwargs)
        confidences.sort(key=lambda x: x[1])
        return confidences[:count]

    def entropy_sampling(
        self, model: torch.nn.Module, count, **kwargs
    ) -> List[Tuple[str, torch.Tensor]]:
        """Max entropy sampling."""
        entropies = []

        def handler(outputs, img_batch, data_size):
            """Handle samples model output.

            :param outputs:
            :param img_batch:
            :param data_size:
            """
            for j in range(data_size):
                _img = img_batch[j]
                probabilities = torch.softmax(outputs[j], 0)
                entropy = 0
                for prob in probabilities:
                    entropy += -prob * math.log(prob, 2)
                entropies.append((_img, entropy))

        self.__sample(model, handler, **kwargs)
        entropies.sort(key=lambda x: x[1], reverse=True)
        return entropies[:count]

    def random_sampling(self, count: int, **kwargs):
        """Handle samples model output.

        :param count: How many files
        :param kwargs: Optional random seed
        """
        random_seed = kwargs.get("random_seed", self._random_seed)
        random.seed(random_seed)
        samples_dir = self.storage.samples_dir(True)
        sample_files = self.storage.samples_dir()
        random.shuffle(sample_files)
        return [(os.path.join(samples_dir, x), None) for x in sample_files[:count]]

    def get_samples(
        self,
        count: int,
        strategy: QueryStrategy,
        model: torch.nn.Module = None,
        **kwargs
    ):
        """Get samples from the samples pool folder.

        @:param count: how many samples
        @:param strategy: how to get these samples
        """
        if strategy == QueryStrategy.Random:
            return self.random_sampling(count)
        if model is None:
            raise ValueError("Model not provided")
        samples = []
        if strategy == QueryStrategy.Margin:
            samples = self.margin_confidence_sampling(model, count, **kwargs)
        if strategy == QueryStrategy.LeastConfidence:
            samples = self.least_confidence_sampling(model, count, **kwargs)
        if strategy == QueryStrategy.Entropy:
            samples = self.entropy_sampling(model, count, **kwargs)
        return samples
