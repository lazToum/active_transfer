# -*- coding: utf-8 -*-
from .scenario import AtScenario, PhaseSize
from .enums import Category, SubCategory


class AtTask(AtScenario):
    """Extend scenario with peers and peer accuracy.

    Implement a scenario, specify the number of required inputs before
    deciding for a class and the accuracy of the user inputs "if 8 out
    of 10 users say this image is of class A, then accept it as class A"
    """

    peers: int = 10
    peer_accuracy: float = 0.5

    def __init__(
        self,
        phase_sizes: PhaseSize,
        append_size: int,
        category: Category,
        subcategory: SubCategory,
        **kwargs
    ):
        """Class initialization.."""
        super(AtTask, self).__init__(
            phase_sizes, append_size, category, subcategory, **kwargs
        )
        self.peers = int(kwargs.get("peers", self.peers))
        self.peer_accuracy = float(kwargs.get("peer_accuracy", self.peer_accuracy))
