# -*- coding: utf-8 -*-

import os
import sys
from datetime import datetime
from enum import Enum
import torch
import torchvision
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from typing import List, Union, Dict
import shutil

try:
    from active_transfer import (
        here,
        ActiveTransfer,
        QueryStrategy,
        PhaseSize,
        Phase,
        Task,
        Category,
        SubCategory,
        weighted_dir_items,
    )

except ImportError:

    sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__), "..")))
    from active_transfer import (
        here,
        ActiveTransfer,
        QueryStrategy,
        PhaseSize,
        Phase,
        Task,
        Category,
        SubCategory,
        weighted_dir_items,
    )


_here = here()
# not in module (active_transfer) requirements
daemon_imported = False
try:
    import daemon

    daemon_imported = True
except ModuleNotFoundError:
    pass

seaborn_imported = False
try:
    import seaborn as sns
    import matplotlib.pyplot as plt
    import csv

    seaborn_imported = True
except ModuleNotFoundError:
    pass


class ExampleConfig(Enum):
    """example run configuration."""

    model_name = f"run-{datetime.now().strftime('%Y-%m-%d-%H-%M-%S')}.pt"
    model = torchvision.models.resnet50(pretrained=True)
    random_seed = 0
    train_size = 20
    test_size = 400
    val_size = 400
    append_size = 20
    num_epochs = 20
    batch_size = 16  # host dependent
    num_workers = 4  # host dependent
    num_loops = 30
    optimizer_lr = 0.001
    optimizer_momentum = 0.9
    scheduler_step = 7
    peers = 20
    # require 16 / 20 same answers per sample to decide for class
    peer_accuracy = 0.8


RUN_AS_DAEMON = True
# simulate peer labeling (existing folder with annotated data)
EXISTING_LABELED_PATH = os.environ.get("ACTIVE_TRANSFER_PEER_DATA", None)
# ants/bees example: https://download.pytorch.org/tutorial/hymenoptera_data.zip
# custom positive/negative (binary) keys/folder names
POSITIVE_CLASS = os.environ.get("AT_POSITIVE", "positive")
NEGATIVE_CLASS = os.environ.get("AT_NEGATIVE", "negative")
FIXED_FEATURE_EXTRACTOR = (
    int(os.environ.get("AT_FIXED_EXTRACTOR", 1)) == 1
)  # or fine_tune the conv_net ?
# replace .tmp with assets/server static files?
DATA_ROOT_DIR = os.path.join(_here, ".tmp")


def notifier(text, *_, **__):
    """
    Update function.

    Just print it here, maybe a websocket update/notification in other examples
    """
    print(text)


def get_dataset_transform(is_train: bool = False):
    """Get dataset transformation.

    :param is_train: Whether is a train dataset or not
    :return:The transform function
    """
    if is_train:
        return torchvision.transforms.Compose(
            [
                torchvision.transforms.RandomResizedCrop(224),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
            ]
        )
    return torchvision.transforms.Compose(
        [
            torchvision.transforms.Resize(256),
            torchvision.transforms.CenterCrop(224),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        ]
    )


def get_labeled_files(count: int) -> Dict[Union[str, os.PathLike], List[str]]:
    """Feed with existing labeled data."""
    total = {}
    if EXISTING_LABELED_PATH is not None and os.path.exists(EXISTING_LABELED_PATH):
        class_names = [POSITIVE_CLASS, NEGATIVE_CLASS]
        partial = weighted_dir_items(
            EXISTING_LABELED_PATH, class_names, count, True, ExampleConfig.random_seed.value
        )
        for class_name, item_names in partial.items():
            # random.seed(ExampleConfig.random_seed.value)
            # random.shuffle(item_names)
            total[class_name] = item_names
    return total


def _setup(phase_sizes: PhaseSize, append_size: int, strategy: QueryStrategy) -> ActiveTransfer:
    """Init params."""
    class_names = [POSITIVE_CLASS, NEGATIVE_CLASS]
    model: torch.nn.Module = ExampleConfig.model.value
    model_name = f"{strategy.name}_{ExampleConfig.model_name.value}"
    models_dir = os.path.join(DATA_ROOT_DIR, "models")
    model_path: str = os.path.join(models_dir, model_name)
    non_train_transform = get_dataset_transform()
    train_transform = get_dataset_transform(is_train=True)
    transforms: Dict[Phase, torchvision.transforms.Compose] = {
        Phase.Train: train_transform,
        Phase.Val: non_train_transform,
        Phase.Sample: non_train_transform,
        Phase.Test: non_train_transform,
    }
    scenario = Task(
        phase_sizes=phase_sizes,
        append_size=append_size,
        category=Category.Image,
        subcategory=SubCategory.Binary,
        strategy=strategy,
        peers=ExampleConfig.peers.value,
        peer_accuracy=ExampleConfig.peer_accuracy.value,
    )

    return ActiveTransfer(
        root_dir=DATA_ROOT_DIR,
        model=model,
        scenario=scenario,
        class_names=class_names,
        notifier=notifier,
        batch_size=ExampleConfig.batch_size.value,
        num_workers=ExampleConfig.num_workers.value,
        model_path=model_path,
        transforms=transforms,
        num_epochs=ExampleConfig.num_epochs.value,
        random_seed=ExampleConfig.random_seed.value,
    )


def _move_all_files(src_dir, dst_dir, find_replace=None):
    """Move all files from src_dir to dst_dir."""
    if (
        os.path.exists(dst_dir)
        and os.path.isdir(dst_dir)
        and os.path.exists(src_dir)
        and os.path.isdir(src_dir)
    ):
        # _replace = (
        #     f" replacing '{find_replace['find']}' with '{find_replace['replace']}'"
        #     if find_replace is not None
        #     else ""
        # )
        # notifier(f"moving all files from {src_dir} to {dst_dir}{_replace}")
        for entry in sorted(os.listdir(src_dir)):
            src = os.path.join(src_dir, entry)
            dst_name = entry
            do_move = True
            if find_replace is not None:
                find = find_replace["find"]
                replace = find_replace["replace"]
                do_move = find in dst_name
                if do_move:
                    dst_name = entry.replace(find, replace)
            if do_move:
                dst = os.path.join(dst_dir, dst_name)
                shutil.move(src=src, dst=dst)


def _move_labeled_files(dst_root: Union[str, os.PathLike], labeled_files: Dict[str, List[str]]):
    """Move labeled files to specified folder."""
    for class_name, class_files_list in labeled_files.items():
        for class_file in class_files_list:
            src = os.path.join(EXISTING_LABELED_PATH, class_name, class_file)
            # /path/to/root/train/{positive|negative}
            dst_dir = os.path.join(dst_root, class_name)
            dst = os.path.join(dst_dir, class_file)
            # print(f"move {src} to {dst}")
            if (
                os.path.exists(src)
                and os.path.isfile(src)
                and os.path.exists(dst_dir)
                and os.path.isdir(dst_dir)
            ):
                shutil.move(src=src, dst=dst)


def _train(at: ActiveTransfer):
    """Call train."""
    if FIXED_FEATURE_EXTRACTOR:
        for param in at.model.parameters():
            param.requires_grad = False
    num_features = at.model.fc.in_features
    at.model.fc = nn.Linear(num_features, 2)
    at.model = at.model.to(at.device)
    criterion = nn.CrossEntropyLoss()
    lr = ExampleConfig.optimizer_lr.value
    momentum = ExampleConfig.optimizer_momentum.value
    params = at.model.fc.parameters() if FIXED_FEATURE_EXTRACTOR else at.model.parameters()
    optimizer = optim.SGD(params, lr=lr, momentum=momentum)
    step_size = ExampleConfig.scheduler_step.value
    scheduler = lr_scheduler.StepLR(optimizer, step_size=step_size)
    return at.train(criterion, optimizer, scheduler)


def _sample(at: ActiveTransfer, csv_dict: dict):
    """Sampling."""
    csv_dict["sampling_start"] = str(datetime.now())
    samples = at.get_samples(ExampleConfig.append_size.value)
    # notifier(f"Samples: {samples}")
    csv_dict["sampling_end"] = str(datetime.now())
    for img_path, _ in samples:
        name, ext = os.path.splitext(img_path)
        #  it is supposed to be unknown
        # and get the class name fro the peers
        # let's "predict" the class from its name("{src_name}_{class_name}{src_extension}")
        file_class = NEGATIVE_CLASS if name.endswith(NEGATIVE_CLASS) else POSITIVE_CLASS
        dst_dir = img_path.replace(
            at.samples_dir(True), os.path.join(at.train_dir(True), file_class)
        )
        dst = dst_dir.replace(f"_{file_class}{ext}", ext)
        shutil.move(img_path, dst)
    return csv_dict


def _loop(model_exists: bool, at: ActiveTransfer):
    """Train - Test - Sample loop."""
    class_names = [POSITIVE_CLASS, NEGATIVE_CLASS]
    csv_dict = {
        "strategy": at.scenario.strategy.name,
        "loop_start": str(datetime.now()),
        "sampling_start": "NA",
        "sampling_end": "NA",
    }
    if not at.have_samples:
        # let's add some of the remaining images to the samples pool
        class_dirs = {x: os.path.join(EXISTING_LABELED_PATH, x) for x in class_names}
        class_files = {x: len(os.listdir(y)) for x, y in class_dirs.items()}
        total = sum([y for y in class_files.values()])
        samples = get_labeled_files(min(total, 2 * ExampleConfig.append_size.value))
        samples_pool = at.samples_dir(only_path=True)
        for class_name, class_files in samples.items():
            for class_file in class_files:
                src = os.path.join(EXISTING_LABELED_PATH, class_name, class_file)
                src_name, src_extension = os.path.splitext(class_file)
                dst_name = f"{src_name}_{class_name}{src_extension}"
                dst = os.path.join(samples_pool, dst_name)
                # notifier(f"Move {src} to {dst}")
                shutil.move(src, dst)
    if model_exists:
        notifier(f"Getting {ExampleConfig.append_size.value} samples to ask for labeling")
        csv_dict = _sample(at, csv_dict)
    csv_dict["train_start"] = str(datetime.now())
    csv_dict["train_count"] = sum(
        len(os.listdir(os.path.join(at.train_dir(True), x))) for x in class_names
    )
    model, last_epoch, min_loss = _train(at)
    torch.save(model, at.model_path)
    csv_dict["train_end"] = str(datetime.now())
    csv_dict["min_loss"] = min_loss
    for phase, epoch_dict in last_epoch.items():
        base = f"last_epoch_{phase}"
        for key, value in epoch_dict.items():
            csv_dict[f"{base}_{key}"] = value
    csv_dict["test_start"] = str(datetime.now())
    test_stats = at.test()
    notifier(f"Tests: {str(test_stats)}")
    for key, value in test_stats.items():
        csv_dict[key] = value
    csv_dict["test_end"] = str(datetime.now())
    csv_dict["loop_end"] = str(datetime.now())
    return csv_dict
    # notifier(str(csv_dict))


def strategy_loop(strategy: QueryStrategy):
    """Strategy loop."""
    train_size = ExampleConfig.train_size.value
    test_size = ExampleConfig.test_size.value
    val_size = ExampleConfig.val_size.value
    append_size = ExampleConfig.append_size.value
    phase_sizes = PhaseSize(
        {Phase.Train.value: train_size, Phase.Test.value: test_size, Phase.Val.value: val_size}
    )
    at = _setup(phase_sizes, append_size, strategy)
    results_dir = os.path.join(DATA_ROOT_DIR, "results")
    os.makedirs(results_dir, exist_ok=True)
    if not at.can_train:
        # let's say we have collected {train_size + val_size + test_size} images
        # so we can have our first train
        train_dir = at.train_dir(True)
        val_dir = at.val_dir(True)
        test_dir = at.test_dir(True)
        for phase_dir, size in {
            train_dir: train_size,
            val_dir: val_size,
            test_dir: test_size,
        }.items():
            labeled = get_labeled_files(size)
            _move_labeled_files(phase_dir, labeled)
    # model_exists = os.path.exists(at.model_path) and os.path.isfile(at.model_path)
    # if model_exists:

    # at.model = torch.load(at.model_path)
    total_loops = ExampleConfig.num_loops.value
    model_exists = False
    for index in range(total_loops):
        notifier(f"Strategy: {strategy.name} - Loop: {index + 1} / {total_loops}")
        csv_dict = _loop(model_exists, at)
        model_name = f"{strategy.name}_{ExampleConfig.model_name.value}"
        report_name = model_name.replace(".pt", ".csv")
        report_path = os.path.join(results_dir, report_name)
        at.write_csv(csv_file=report_path, entries=csv_dict)
        model_exists = True
    del at
    if torch.cuda.is_available():
        torch.cuda.empty_cache()


def rollback():
    """Move any files back to where they were."""
    ...
    phases = [Phase.Train, Phase.Val, Phase.Test, Phase.Sample]
    class_names = [NEGATIVE_CLASS, POSITIVE_CLASS]
    for phase in phases:
        phase_dir = os.path.join(DATA_ROOT_DIR, phase.value)
        if os.path.exists(phase_dir) or True:
            for class_name in class_names:
                src_dir = (
                    os.path.join(phase_dir, class_name) if phase is not Phase.Sample else phase_dir
                )
                dst_dir = os.path.join(EXISTING_LABELED_PATH, class_name)
                find_replace = (
                    {"find": f"_{class_name}", "replace": ""} if phase is Phase.Sample else None
                )
                _move_all_files(src_dir, dst_dir, find_replace)


def _plot(results):
    results_dir = os.path.join(DATA_ROOT_DIR, "results")
    if seaborn_imported and len(results) > 0:
        plt.figure(figsize=(16, 9))
        sns.set(style="darkgrid", rc={"lines.linewidth": 3}, font_scale=1.75)
        sns.lineplot(
            x="train_count",
            y="test_acc",
            data=results,
            style="strategy",
            hue="strategy",
            dashes=False,
        )
        sns.lineplot(x="train_count", y="test_acc", data=results, color="b", linewidth=0)
        plt.xlabel("Train Size", fontsize=24)
        plt.ylabel("Model Accuracy", fontsize=24)
        if as_daemon:
            save_name = ExampleConfig.model_name.value.replace(".pt", ".pdf")
            save_path = os.path.join(results_dir, save_name)
            plt.savefig(save_path, bbox_inches="tight")
            save_path = save_path.replace(".pdf", ".png")
            plt.savefig(save_path, bbox_inches="tight")
        else:
            plt.show()


def main():
    """base."""
    if EXISTING_LABELED_PATH is not None:
        rollback()
        results = {}
        results_dir = os.path.join(DATA_ROOT_DIR, "results")
        for strategy in [
            QueryStrategy(QueryStrategy.Random),
            # QueryStrategy(QueryStrategy.LeastConfidence),
            # QueryStrategy(QueryStrategy.Margin),
            # un-comment above to include the other strategies,
            # they do provide the same results though (in the case of binary classification)
            QueryStrategy(QueryStrategy.Entropy),
        ]:
            strategy_loop(strategy)
            notifier("Rolling back ....")
            rollback()
            model_name = f"{strategy.name}_{ExampleConfig.model_name.value}"
            report_name = model_name.replace(".pt", ".csv")
            report_path = os.path.join(results_dir, report_name)
            if os.path.exists(report_path) and seaborn_imported:
                with open(report_path, newline="") as csv_read:
                    reader = csv.DictReader(csv_read)
                    if "strategy" not in results:
                        results["strategy"] = []
                    for row in reader:
                        results["strategy"].append(strategy.value)
                        for key in ["train_count", "test_acc"]:
                            if key not in results:
                                results[key] = []
                            _value = row.get(key)
                            if key == "test_acc":
                                _value = float(_value)
                            else:
                                _value = int(_value)
                            results[key].append(_value)
                _plot(results)
            else:
                print(report_path)


if __name__ == "__main__":
    as_daemon = int(os.environ.get("AT_DAEMON", RUN_AS_DAEMON))
    if as_daemon == 1 and daemon_imported:
        logs_dir = os.path.join(DATA_ROOT_DIR, "logs")
        os.makedirs(logs_dir, exist_ok=True)
        log_file = os.path.join(logs_dir, "test.log")
        _stdout = sys.stdout
        _stderr = sys.stderr
        with open(log_file, "w") as open_log:
            sys.stdout = open_log
            sys.stderr = open_log
            with daemon.DaemonContext(working_directory=_here, stdout=open_log, stderr=open_log):
                main()
            sys.stdout = _stdout
            sys.stderr = _stderr
            open_log.close()
    else:
        main()
